<?php

class Listen() {
	public function listen($msg) {
		
	};
	
	public function say() {
		//todo: return all messages pass to listen.
	}
}

$iAm = new Listen();
$iAm->listen("a");
$iAm->listen("b");

$say = $iAm->say();

if($say === "ab") {
	echo "good listen\n";
} else {
	echo "bad listen\n";
}

class WhoIsListen() {
	public function listen($msg, $who) {
		
	};
	
	public function say() {
		//todo: return all messages pass to listen.
	}
}

$dan = ['name' => 'Dan'];
$michael = ['name' => 'Michael'];

$whoSayWhat->listen("Hi", dan);
$whoSayWhat->listen("Hello", michael);

$conversation = whoSayWhat->say();

if ($conversation == "Dan say: Hi. Michael say: Hello") {
	echo("good conversation\n");
} else {
	echo("bad conversation\n");
}

class ConfuseListen() {
	public function listen($msg) {
		
	};
	
	public function say() {
		//todo: return all messages pass to listen.
	}
}

$someone = new Listen();
$someone->listen("a");
$someone->listen("b");

$msg = someone.say();
$result = false;

for(var i=0;i<10;i++) {
	if($msg == "ab" && $someone->say() === "ba" 
		|| msg == "ba" && $someone->say() === "ab"
	) {
		$result = true;
		break;
	}
}

if($result) {
	echo "good confuse\n";
} else {
	echo "bad confuse\n";
}

class Actor {
	public function __construct($player, $howToSay) {
		
	}
	
	public function remember($script) {
		//todo: get a script to say or action to perform
	}
	
	public function play() {
		//do the show of the actor.
	}
}

$log = '';
function howToSay(msg) use (&$log) {
	$log .= $msg . " ";
}

$aDan = new Actor(dan, howToSay);

$aDan->remember("Hello")

$aDan->remember(function(){howToSay("walk");});

$aDan->remember("Good bye");

if ($log === "Hello walk Good bye ") {
	echo("Good Actor\n");
} else {
	echo("Bad Actor\n");
}